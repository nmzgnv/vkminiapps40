import React, {useState} from "react";
import {Button} from "@vkontakte/vkui";


const ManageButton = (props) => {
  const [isOn , setIsOn] = useState(props.isOn || 0);
  const buttonStyle = {width: 36, marginRight: 5}

  const changeButtonClicked = () => {
    const newValue = (isOn + 1) % 2;
    props.setLight(newValue);
    setIsOn(newValue);
  }

    if (isOn){
      return(
        <Button mode="commerce" onClick={changeButtonClicked} style={buttonStyle}>
          Off
        </Button>
      )
    }

  return (
    <Button mode="destructive" onClick={changeButtonClicked} style={buttonStyle}>
      On
    </Button>
  )
}

export default ManageButton;


