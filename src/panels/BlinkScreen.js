import React, {useEffect, useState} from "react";
import {Button, Div, Group, Header, Panel, PanelHeader, Text} from "@vkontakte/vkui";
import bridge from '@vkontakte/vk-bridge';
import {LightValues} from "../constants/LightValues";
import ManageButton from "../components/ManageButton";


const BlinkScreen = ({id}) => {
  const [debugText, setDebugText] = useState('text: ');
  const [lanternAvailable, setLanternAvailable] = useState(false);
  const [blinkStarted, setBlinkStarted] = useState(false);
  const [lanternStateIndex, setLanternStateIndex] = useState(-1);
  const [lanternStates, setLanternStates] = useState([
    LightValues.on,
    LightValues.off,
    LightValues.on,
    LightValues.off,
    LightValues.on,
    LightValues.off,
    LightValues.on,
    LightValues.off,
  ]);
  const startButtonStyle = {marginLeft: 15, width: 160}

  const debug = (text) => {
    // Debug is off to optimization
    return;
    setDebugText(prevText => prevText +' ' + text);
  }

  useEffect(() => {
    bridge.send("VKWebAppInit").then(
      json => debug('app initialized', json.result)
    );

    bridge.send("VKWebAppFlashGetInfo").then(json => {
        if (!json){
          debug(JSON.stringify(json) + 'is empty');
          return;
        }
        console.log(json);
        debug(JSON.stringify(json));
        if (json.hasOwnProperty('is_available')){
          debug('lantern can be enabled [json.is_available]', json.is_available.toString())
          setLanternAvailable(json.is_available);
        } else if (json.hasOwnProperty('data') && json.data.hasOwnProperty('is_available')){
          debug('lantern can be enabled [json.data.is_available]', json.data.is_available)
          setLanternAvailable(json.data.is_available);
        } else if (json.hasOwnProperty('error_type')){
          debug('lantern get info error', json.error_type)
        } else if (json.hasOwnProperty('data') && json.data.hasOwnProperty('error_type')){
          debug('lantern get info error', json.data.error_type)
        } else {
          debug('not documented ' + '-' + JSON.stringify(json))
        }
    }).catch(error => debug(error));

  }, [])

  const tryEnableLantern = () => {
    bridge.send("VKWebAppFlashSetLevel", {"level": 1}).then(json => {
      debug('lantern enable ' + JSON.stringify(json));
    }).catch(err => debug('lantern enable error ' + err))
  }

  const tryDisableLantern = () => {
    bridge.send("VKWebAppFlashSetLevel", {"level": 0}).then(json => {
      debug('lantern disable ' + JSON.stringify(json));
    }).catch(err => debug('lantern disable error ' + err))
  }


  useEffect(() => {
    if (lanternAvailable && blinkStarted) {
      let timer = setInterval(() => {
        const enableLantern = lanternStates[lanternStateIndex];
        enableLantern && blinkStarted ? tryEnableLantern() : tryDisableLantern();
        debug('lantern status changed');
        debug('next index ' + lanternStateIndex);
        setLanternStateIndex((lanternStateIndex + 1) % lanternStates.length);
      }, 1000);
      return () => clearInterval(timer);
    } else if (!blinkStarted){
      tryDisableLantern();
    }
  }, [lanternStateIndex]);

  useEffect(() => {
    if (lanternAvailable && blinkStarted) {
      debug('lantern is available. Running...\n')
      setLanternStateIndex(0);
    }
  }, [blinkStarted]);

  const setLanternState = (index, value) => {
    let temporaryValues = lanternStates;
    temporaryValues[index] = value
    setLanternStates(temporaryValues);
  }

  return(
    <Panel id={id}>
      <PanelHeader>Дискотека</PanelHeader>

      <Text style={{textAlign: 'center', marginTop: 10, color: '#e64646', fontWeight: 500}}>
        {lanternAvailable ? '' : 'У тебя точно есть фонарик? Я не смогу зажечь :('}
      </Text>

      <Group>
        <Group header={<Header mode="secondary">Настройка фонарика</Header>}>
          <Div style={{flexDirection:'row'}}>
            {lanternStates && lanternStates.length > 0 ? (
              lanternStates.map((item, key) => <ManageButton isOn={item === LightValues.on}
                                                           setLight={(newValue) => setLanternState(key, newValue)}/>)
            ): <Div/>}
          </Div>
        </Group>

        {blinkStarted ? (
          <Button mode="destructive"
                  onClick={() => {
                    setBlinkStarted(false);
                    tryDisableLantern();
                  }}
                  stretched={true}
                  style={startButtonStyle}
          >
            Закончить(
          </Button>
        ) : (
          <Button
            mode='primary'
            stretched={true}
            onClick={() => setBlinkStarted(true)}
            style={startButtonStyle}
          >
            Начать веселье
          </Button>
        )}
      </Group>
    </Panel>
  )
}


export default BlinkScreen
