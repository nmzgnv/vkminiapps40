import React, {useEffect, useState} from "react";
import {Button, Div, Group, Header, Panel, PanelHeader, Text} from "@vkontakte/vkui";
import {connectToDj, getDjList, getSessionList} from "../utils";


const DjListScreen = ({id, user}) => {
  const [djList, setDjList] = useState([]);

  useEffect(() => {
    getDjList().then(list => {
      console.log(list);
      setDjList(list);
    });
  }, [])

  return(
    <Panel id={id}>
      <PanelHeader>Список Dj</PanelHeader>
      <Group style={{paddingLeft: 20}}>
        {djList && djList.length > 0 ? (
          djList.map((item, key) => <Button onClick={() => connectToDj(user, item)} key={key}>{item.user_id}</Button>)
        ) :
          <Text weight="regular">Список пуст</Text>
        }
      </Group>
    </Panel>
  )
}

export default DjListScreen
