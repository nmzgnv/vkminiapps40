import React, {useEffect, useState} from "react";
import {Alert, Button, Div, Group, Header, Panel, PanelHeader, Text} from "@vkontakte/vkui";
import bridge from '@vkontakte/vk-bridge';
import {LightValues} from "../constants/LightValues";
import ManageButton from "../components/ManageButton";
import {getSessionList, saveLanternStates} from "../utils";


const DjScreen = ({id, user}) => {
  const [lanternStates, setLanternStates] = useState([
    LightValues.on,
    LightValues.off,
    LightValues.on,
    LightValues.off,
    LightValues.on,
    LightValues.off,
    LightValues.on,
    LightValues.off,
  ]);
  const startButtonStyle = {marginLeft: 15, width: 160, marginBottom: 20,}

  const setLanternState = (index, value) => {
    let temporaryValues = lanternStates;
    temporaryValues[index] = value
    setLanternStates(temporaryValues);
  }

  useEffect(() => {
    bridge.send("VKWebAppGetUserInfo").then(result => {
      const json = JSON.parse(result);
      setUser(json);
    });
  }, [])

  const setLanternOn = async () => {
    saveLanternStates(user, lanternStates, true);
    const sessions = await getSessionList();
    const filteredSessions = sessions.filter(item => item.connectedTo === user.id);
    if (!filteredSessions || filteredSessions.length <= 0){
      console.log('no connected users found!');
      return;
    }
    console.log('connected users: ', filteredSessions);
  }

  return(
    <Panel id={id}>
      <PanelHeader>Dj пульт</PanelHeader>
      <Group>
        <Group header={<Header mode="secondary">Настройка фонарика</Header>}>
          <Div style={{flexDirection:'row'}}>
            {lanternStates && lanternStates.length > 0 ? (
              lanternStates.map((item, key) => <ManageButton isOn={item === LightValues.on}
                                                           setLight={(newValue) => setLanternState(key, newValue)}/>)
            ): <Div/>}
          </Div>
        </Group>

        <Button
            mode='primary'
            stretched={true}
            onClick={() => saveLanternStates(user, lanternStates)}
            style={startButtonStyle}
        >
            Сохранить
        </Button>
        <Button
          mode='primary'
          stretched={true}
          onClick={() => setLanternOn()}
          style={startButtonStyle}
        >
          Запустить дискотеку
        </Button>

      </Group>
    </Panel>
  )
}


export default DjScreen
