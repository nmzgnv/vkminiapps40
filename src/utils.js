import bridge from "@vkontakte/vk-bridge";

export const StorageKeys = {
  DjList: "DJList",
  SessionList: "Sessions",
}

export const saveLanternStates = async (user, states, enabled= false) => {
  let djList = await getDjList();

  djList.push({
    user_id: user.id,
    user_name: user.first_name + " " + user.last_name,
    lanternStates: states,
    enabled: enabled,
  })

  const result = await bridge.send("VKWebAppStorageSet", {
    key: StorageKeys.DjList,
    value: JSON.stringify(djList),
  });

  console.log(result);
}

export const getDjList = async () => {
  const data = await bridge.send("VKWebAppStorageGet", {"keys": [StorageKeys.DjList]});
  console.log(data.keys[0].value);
  const keyData = data.keys[0]?.value;

  if (!keyData || keyData.length <= 0){
    return [];
  }

  return JSON.parse(keyData);
}


export const getSessionList = async () => {
  const data = await bridge.send("VKWebAppStorageGet", {"keys": [StorageKeys.SessionList]});
  // console.log(data.keys[0].value);
  console.log('sessions', data);
  const keyData = data.keys[0]?.value;
  if (!keyData || keyData.length <= 0){
    return [];
  }
  return JSON.parse(keyData);
}

export const connectToDj = async (user, dj) => {
  console.log('connecting to session...');
  let sessions = await getSessionList();
  console.log(sessions);
  sessions = sessions.filter(item => item.user_id !== user.id);
  sessions.push({
    user_id: user.id,
    connectedTo: dj.user_id,
    lanternStates: dj.lanternStates,
  });

  const answer = await bridge.send("VKWebAppStorageSet", {
    key: StorageKeys.SessionList,
    value: JSON.stringify(sessions),
  });

  console.log('connect to session result is', answer);
  return answer?.result;
}
