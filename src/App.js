import React, { useState, useEffect } from 'react';
import bridge from '@vkontakte/vk-bridge';
import {View, ScreenSpinner, AdaptivityProvider, AppRoot, Button, Tabbar, TabbarItem, Epic} from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import BlinkScreen from "./panels/BlinkScreen";
import DjListScreen from "./panels/DjListScreen";
import DjScreen from "./panels/DjScreen";
import {getSessionList} from "./utils";
import SessionScreen from "./panels/SessionScreen";

const App = () => {
	const [activePanel, setActivePanel] = useState('djListScreen');
	const [fetchedUser, setUser] = useState(null);
	const [popout, setPopout] = useState(<ScreenSpinner size='large' />);
	const [activeStory, setActiveStory] = React.useState('start');
	const [isSessionStarted, setSessionStarted] = useState(false);
	const [sessionLanternStates, setLanternStates] = useState([0, 0, 0, 0, 0, 0, 0, 0, 0]);

	useEffect(() => {
		bridge.subscribe(({ detail: { type, data }}) => {
			if (type === 'VKWebAppUpdateConfig') {
				const schemeAttribute = document.createAttribute('scheme');
				schemeAttribute.value = data.scheme ? data.scheme : 'client_light';
				document.body.attributes.setNamedItem(schemeAttribute);
			}
			// приложение закрыто
			if (type === 'VKWebAppViewHide'){

			}
		});
		async function fetchData() {
			const user = await bridge.send('VKWebAppGetUserInfo');
			setUser(user);
			setPopout(null);
		}
		fetchData();
	}, []);


	useEffect(() => {
		let timer = setInterval(() => {
			console.log('checking for session start');
			getSessionList().then(sessions => {
				const mySessions = sessions.filter(s => s.user_id === fetchedUser?.id);
				if (!mySessions || mySessions.length <= 0 || isSessionStarted){
					setLanternStates([]);
					return;
				}
				const currentSession = mySessions[0];
				if (currentSession.enabled){
					setLanternStates(currentSession.lanternStates);
					setActivePanel('SessionScreen');
				} else {
					setLanternStates([]);
				}
				// lanternStates
			})
		}, 1000);
		return () => clearInterval(timer)
	}, [])

	const go = e => {
		setActivePanel(e.currentTarget.dataset.to);
	};

	return (
		<AdaptivityProvider>
			<AppRoot>
				<View activePanel={activePanel} popout={popout}>
					<BlinkScreen id='blinkScreen'/>
					<DjListScreen id='djListScreen' user={fetchedUser}/>
					<SessionScreen id='SessionScreen' lanternStates={sessionLanternStates}/>
					<DjScreen id='djScreen' user={fetchedUser}/>
				</View>
				<View>
					<Epic activeStory={activeStory} tabbar={
						<Tabbar>
							<TabbarItem
								onClick={() => {
									setActivePanel('djScreen');
								}}
								selected={activePanel === 'djScreen'}
								text="Панель Dj"
							/>
							<TabbarItem
								onClick={() => {
									setActivePanel('djListScreen');
								}}
								selected={activePanel === 'djListScreen'}
								text="Список Dj"
							/>
						</Tabbar>
					}/>
				</View>
			</AppRoot>
		</AdaptivityProvider>
	);
}

export default App;
